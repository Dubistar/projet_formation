<?php
require 'header.php';
?>

<h2>Se connecter</h2>

<form action="connexion.php" method="GET" onsubmit="return connexion()">
  Login : <input type="text" id="login" name="login"><div id="videlogin"></div><br>
  Password : <input type="password" id="password" name="password"><div id="videmdp"></div><br>
  <input type="submit" value="valider">
</form>

<hr>

<h2>Créer un compte</h2>

<form action="inscription.php" method="GET" onsubmit="return inscription()">
  Login : <input type="text" id="login1" name="login1"><div id="videlogin1"></div><br>
  Email : <input type="email" id="email" name="email"><div id="videmail"></div><br>
  Password : <input type="password" id="password1" name="password1"><div id="videmdp1"></div><br>
  Confirm Password : <input type="password" name="passConfirm" id="passConfirm" ><br>
  <div id="msgStr"style="color:#FF0000"></div><div id="msgMdp"style="color:#FF0000"></div>
  <input type="submit" value="valider" >
</form>
<br>

 <a href="index.php">Retour à l'accueil</a>

<script>
  function test(idInput,idvide,idmessage){
    if(document.getElementById(idInput).value ==""){
      document.getElementById(idvide).innerHTML="Le "+idmessage+" n'est pas renseigné";
      return true
    }else{
      document.getElementById(idmessage).innerHTML="";
      return false
    }
  }

  function connexion(){
  error1=test("login","videlogin","login");
  error2=test("password","videmdp","password");

    if (error1 || error2){
      return false
    }else{
      return true
    }
  }

  function verifpass(){
   str = document.getElementById("password1").value.length;
   valpass = document.getElementById("password1").value
   valpassConfirm = document.getElementById("passConfirm").value

   if (str<7) {
     document.getElementById("password1").style.border= "1px solid #FF0000";
     document.getElementById("passConfirm").style.border= "1px solid #FF0000";
     document.getElementById("msgStr").innerHTML="Vous devez remplir 7 lettres minimum";
     return true
   }else if (valpass!=valpassConfirm) {
     document.getElementById("password1").style.border= "1px solid #FF0000";
     document.getElementById("passConfirm").style.border= "1px solid #FF0000";
     document.getElementById("msgMdp").innerHTML="Pass différent";
     return true
   }else{
     document.getElementById("password1").style.border= "";
     document.getElementById("passConfirm").style.border= "";
     document.getElementById("msgMdp").innerHTML="";
     document.getElementById("msgStr").innerHTML="";
     return false
   }
  }

  function inscription(){
  error3=test("login1","videlogin1","login1");
  error4=verifpass()
  error5=test("email","videmail","email");


    if (error3|| error4|| error5){
      return false
    }else{
      return true
    }
  }
</script>
