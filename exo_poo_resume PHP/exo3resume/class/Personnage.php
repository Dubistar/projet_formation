<?php

class Personnage{

  private $_pseudo;
  private $_niveau;
  private $_competence;
  private $_vie;
  private $_mana;
  private $_arme;
  public static $_all = array();



  public function __construct($pseudo,$niveau,$competence,$vie,$mana){
    $this->setPseudo($pseudo);
    $this->setNiveau($niveau);
    $this->setCompetence($competence);
    $this->setVie($vie);
    $this->setMana($mana);


    $this->setPseudo($pseudo);
    self::$_all[]=$this;


  }

  public static function getAll()
  {
      foreach(self::$_all as $perso){
        echo "<p>".$perso->getPseudo()." lui reste ".$perso->getVie()." de PV </p>";

      }
  }

    /**
     * Get the value of Pseudo
     *
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->_pseudo;
    }

    /**
     * Set the value of Pseudo
     *
     * @param mixed $_pseudo
     *
     * @return self
     */
    public function setPseudo($_pseudo)
    {
        $this->_pseudo = $_pseudo;

        return $this;
    }

    /**
     * Get the value of Niveau
     *
     * @return mixed
     */
    public function getNiveau()
    {
        return $this->_niveau;
    }

    /**
     * Set the value of Niveau
     *
     * @param mixed $_niveau
     *
     * @return self
     */
    public function setNiveau($_niveau)
    {
        $this->_niveau = $_niveau;

        return $this;
    }

    /**
     * Get the value of Competence
     *
     * @return mixed
     */
    public function getCompetence()
    {
        return $this->_competence;
    }

    /**
     * Set the value of Competence
     *
     * @param mixed $_competence
     *
     * @return self
     */
    public function setCompetence($_competence)
    {
        $this->_competence = $_competence;

        return $this;
    }

    /**
     * Get the value of Vie
     *
     * @return mixed
     */
    public function getVie()
    {
        return $this->_vie;
    }

    /**
     * Set the value of Vie
     *
     * @param mixed $_vie
     *
     * @return self
     */
    public function setVie($_vie)
    {
        $this->_vie = $_vie;

        return $this;
    }

    /**
     * Get the value of Mana
     *
     * @return mixed
     */
    public function getMana()
    {
        return $this->_mana;
    }

    /**
     * Set the value of Mana
     *
     * @param mixed $_mana
     *
     * @return self
     */
    public function setMana($_mana)
    {
        $this->_mana = $_mana;

        return $this;
    }

    /**
    * Get the value of Arme
    *
    * @return mixed
    */
   public function getArme()
   {
       return $this->_arme;
   }

   /**
    * Set the value of Arme
    *
    * @param mixed $_arme
    *
    * @return self
    */
   public function setArme($_arme)
   {
       $this->_arme = $_arme;

       return $this;
   }



    public function infoPerso(){
      $pv=$this->getVie();
      if ($pv>1) {
        return "<p>".$this->getPseudo()." lvl ".$this->getNiveau().", Niveau compétence ".$this->getCompetence().", ".$this->getVie()." Point de vie et ".
        $this->getMana()." Point de Mana </p>";
      }else{
        return "<p>".$this->getPseudo()." lvl ".$this->getNiveau().", Niveau compétence ".$this->getCompetence().", est parti retrouver les anges </p>";
      }

    }

    public function boirePotionDeVie($quantite){
      $Pv = $this->getVie();
      $NewPv = $Pv + $quantite;
      $this ->setVie($NewPv);
      return "<p>".$this->getPseudo()." a bu une potion qui lui a redonné ".$quantite." PV </p>";
    }

    public function recevoirDegats($qteDegats){
      $Pv = $this->getVie();
      $NewPv = $Pv - $qteDegats;
      $this ->setVie($NewPv);
      if ($NewPv<1) {
        echo "<p>".$this->getPseudo()." est mort suite a une longue agonie! </p>";
      }
    }

    public function attaquer(Personnage $cible){
      $PV = $cible -> getVie();
      $nomCible = $cible ->getPseudo();
      $downPv = $this->getArme()->getDegat();
      $newPv = $PV-$downPv;
      $cible -> setVie($newPv);
      $arme = $this->getArme()->getNomArme();
      if ($newPv<1) {
        echo "<p>".$this->getPseudo(). " à tué ".$nomCible."</p>";
      }else{
        echo "<p>".$this->getPseudo()." a attaqué ".$nomCible." avec ".$arme." et lui a retiré ".$downPv." de PV </p>";
      }
    }
    public function vivant(){
      $pseudo = $this->getPseudo();
      $Vie = $this->getVie();
      if ($Vie<1) {
        echo "<p>".$pseudo." est mort ! </p>";
      }else{
        echo "<p>".$pseudo." est vivant et il lui reste ".$Vie."Pv </p>";
      }
    }

    public function changeArme($arme){
      $this->setArme($arme);
      return "<p>".$this->getPseudo()." à changé d'arme et à pris ".$this->getArme()->getNomArme()." </p>";
    }

    // public function trouverArme($nomArme,$degat){
    //   $Arme1 = new Armes($nomArme,$degat);
    //   echo $this->getPseudo()." à touver ".$nomArme;
    // }



}



 ?>
