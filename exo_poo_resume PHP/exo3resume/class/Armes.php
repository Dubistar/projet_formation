<?php
class Armes{

    private $_nomArme;
    private $_degat;

    public function __construct($nomArme,$degat){
      $this->setNomArme($nomArme);
      $this->setDegat($degat);
    }
    /**
     * Get the value of Nom
     *
     * @return mixed
     */
    public function getNomArme()
    {
        return $this->_nomArme;
    }

    /**
     * Set the value of Nom
     *
     * @param mixed $_nom
     *
     * @return self
     */
    public function setNomArme($_nomArme)
    {
        $this->_nomArme = $_nomArme;

        return $this;
    }

    /**
     * Get the value of Propriete
     *
     * @return mixed
     */
    public function getDegat()
    {
        return $this->_degat;
    }

    /**
     * Set the value of Propriete
     *
     * @param mixed $_propriete
     *
     * @return self
     */
    public function setDegat($_degat)
    {
        $this->_degat = $_degat;

        return $this;
    }

    public function infoArme(){
      return "<p>A trouvé ".$this->getNomArme()." qui provoque dégats ".$this->getDegat()." en PV</p>";
    }

}

 ?>
