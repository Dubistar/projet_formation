<?php

class Personnage{

  private $_nom;
  private $_pv = 100;
  private $_arme;

  public function __construct($nom,$arme){

    $this->setNom($nom);
    $this->setArme($arme);

  }

  /**
   * Get the value of Nom
   *
   * @return mixed
   */
  public function getNom()
  {
      return $this->_nom;
  }

  /**
   * Set the value of Nom
   *
   * @param mixed $_nom
   *
   * @return self
   */
  public function setNom($_nom)
  {
      $this->_nom = $_nom;

      return $this;
  }

  /**
   * Get the value of Pv
   *
   * @return mixed
   */
  public function getPv()
  {
      return $this->_pv;
  }

  /**
   * Set the value of Pv
   *
   * @param mixed $_pv
   *
   * @return self
   */
  public function setPv($_pv)
  {
      $this->_pv = $_pv;

      return $this;
  }

  /**
   * Get the value of Arme
   *
   * @return mixed
   */
  public function getArme()
  {
      return $this->_arme;
  }

  /**
   * Set the value of Arme
   *
   * @param mixed $_arme
   *
   * @return self
   */
  public function setArme($_arme)
  {
      $this->_arme = $_arme;

      return $this;
  }


  public function infoPerso(){

    $arme = $this->getArme()->getNomArme();
    return "Nom : ".$this->getNom()." PV :".$this->getPV()." et porte ".$arme."<br>";

  }

  public function coup($cible){
    $PV = $cible -> getPv();
    $nomCible = $cible ->getNom();
    $downPv = $this->getArme()->getDegat();
    $newPv = $PV-$downPv;
    $cible -> setPv($newPv);
    $arme = $this->getArme()->getNomArme();
    return $this->getNom()." a attaqué ".$nomCible." avec ".$arme." <br>";
  }


  public function changeArme($arme){
    $this->setArme($arme);
    return $this->getNom()." à changé d'arme et à pris ".$this->getArme()->getNomArme()." <br>";
  }




}


 ?>
