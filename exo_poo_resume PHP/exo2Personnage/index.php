<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>




  <?php
  spl_autoload_register('monChargeur');
  function monChargeur($class)
  {
    require 'class/'.$class.'.php'; // On inclut la classe correspondante au paramètre passé.
  }

  $arme1 = new Armes('une épée', 20);
  $arme2 = new Armes('une dague',10);

  $perso1 = new Personnage('Atila',$arme1);
  $perso2 = new Personnage('Biboule',$arme2);

  echo $perso1 -> coup($perso2);
  echo $perso2 -> coup($perso1);

  echo $perso1 ->infoPerso();
  echo $perso2 ->infoPerso();

  echo $perso2 -> changeArme($arme1);
  echo $perso2 -> coup($perso1);

  echo $perso1 ->infoPerso();
  echo $perso2 ->infoPerso();

  echo $perso2 -> changeArme($arme2);
  echo $perso2 -> coup($perso1);

  echo $perso1 ->infoPerso();
  echo $perso2 ->infoPerso();















?>

</body>
</html>
