<?php

class ballon{
  private $_nom;
  private $_couleur;

  public function __construct($nom,$couleur){
    $this->setNom($nom);
    $this->setCouleur($couleur);
  }

      /**
     * Get the value of Nom
     *
     * @return mixed
     */
    public function getNom()
    {
        return $this->_nom;
    }

    /**
     * Set the value of Nom
     *
     * @param mixed $_nom
     *
     * @return self
     */
    public function setNom($_nom)
    {
        $this->_nom = $_nom;

        return $this;
    }

    /**
     * Get the value of Couleur
     *
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->_couleur;
    }

    /**
     * Set the value of Couleur
     *
     * @param mixed $_couleur
     *
     * @return self
     */
    public function setCouleur($_couleur)
    {
        $this->_couleur = $_couleur;

        return $this;
    }

    public function getInfos(){
      return "Ballon nommé : ".$this->getNom()." à pour couleur :".$this->getCouleur()."<br>";
    }

    public function changeCouleur($couleur,Ballon $cible){
        $cible -> setCouleur($couleur);

      }

}

 ?>
